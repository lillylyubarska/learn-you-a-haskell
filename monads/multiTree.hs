import Control.Applicative
import Control.Monad (liftM, ap)

data Tree a = Leaf a | Node a [Tree a]  deriving (Show)

instance Functor Tree  where
  fmap f (Leaf a) = Leaf (f a) 
  fmap f (Node a xs) = Node (f a) [f <$> x | x <- xs] 

instance Applicative Tree  where
  pure  = Leaf
  Leaf f <*> Leaf a = f <$> Leaf a
  Leaf f <*> (Node x xs) = Node (f x) [f <$> x | x <- xs]
  (Node f _) <*> Leaf a = f <$> Leaf a
  (Node f fs) <*> (Node a xs) = Node (f a) [y <*> x | y <- fs, x <- xs] 

instance Monad Tree where 
   return = Leaf
   Leaf x >>= f = f x
   (Node a xs) >>= f = Node b ([(x >>= f) | x <- xs])
		where (Leaf b) = f a

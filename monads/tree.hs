import Control.Applicative
import Control.Monad (liftM, ap)

data Tree a = Leaf a | Node a (Tree a) (Tree a) deriving (Show)

instance Functor Tree  where
  fmap f (Leaf a) = Leaf (f a) 
  fmap f (Node a l r) = Node (f a) (fmap f l) (fmap f r)

instance Applicative Tree  where
  pure  = Leaf
  Leaf f <*> Leaf a = Leaf (f a)
  Leaf f <*> Node a l r = Node (f a) (fmap f l) (fmap f r) 

import Data.List (find)
import Control.Applicative
import Control.Monad (liftM, ap, join)

data Node a = File a | Folder a [Node a]  deriving (Show, Eq) 

instance Functor Node where
  fmap = liftM

instance Applicative Node where
  pure  = return
  (<*>) = ap

instance Monad Node where
  return = File 
  (File x) >>= f = f x
  (Folder a xs) >>= f = Folder x [y >>= f | y <- xs]
                        where (File x) = f a 
  
findByName :: Node String -> String -> Maybe (Node String)
findByName (File a) path = if path == a then  Just (File a) else Nothing     
findByName (Folder a xs) path = if path == a then  Just (Folder a xs) else join (find (/= Nothing) ys)
						where ys = [(findByName x path) | x <- xs]

getName :: Node String -> String
getName (File a) = a
getName (Folder a _) = a 

ls :: Node String -> Maybe [String]
ls (File a) = Nothing
ls (Folder a xs) = Just [getName x | x <- xs]

fs :: Node String
fs = Folder "/" [File "config.ini", File "bla.mp3", File "porn.avi"]
		  
main = do
	print $ findByName fs "config.ini"
        print $ ls fs 
        

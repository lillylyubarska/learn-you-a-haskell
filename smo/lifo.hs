type Order = Int
type Duration = Int
type Task = (Order, Duration)

indexed :: [Int] -> [(Order, Duration)]
indexed [] = []
indexed xs = zip [1..] xs


runTask :: (Task) -> [String] -> [String]
runTask (id, duration) acc = steps ++ acc 
				where taskData = "TASK # " ++ show id
			              steps = take duration (repeat taskData)

simulate :: [Int] -> [String]
simulate xs = foldr runTask [] (indexed xs)


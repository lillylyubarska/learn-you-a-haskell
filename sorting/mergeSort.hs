import Data.List

half :: [a] -> ([a], [a])
half [x, y] = ([x], [y]) 
half xs = (take l xs, drop l xs) 
	where l = length xs `div` 2 

merge :: (Ord a) => ([a], [a]) -> [a]
merge(xs, []) = xs
merge ([], xs) = xs
merge ([x], [y]) = if x > y then [y, x] else [x, y] 
merge (xs, ys) = merge([minXs], [minYs]) ++ merge(remainedXs, remainedYs)
		   where 
		         minXs = minimum xs
			 minYs = minimum ys
	                 remainedXs = delete minXs xs
			 remainedYs = delete minYs ys   

sort' :: (Ord a) => [a] -> [a]
sort' [] = []
sort' [x] = [x]
sort' xs =  merge (sort' left,  sort' right)
	where parts = half xs
	      left = fst parts
	      right = snd parts

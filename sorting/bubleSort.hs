swapList :: (Ord a) => [a] -> [a]
swapList [] = []
swapList [x] = [x]
swapList (x:y:ys)
	| x > y = [y] ++ swapList(x:ys)  
	| otherwise = [x] ++ swapList(y:ys)  

sort :: (Ord a) => [a] -> [a]
sort [] = []
sort [x] = [x]
sort xs = sort(swapList(init ys)) ++ [last ys] where ys = swapList xs 

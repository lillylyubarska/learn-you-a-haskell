replaceMin :: (Ord a) => a -> [a] -> [a]
replaceMin x [] = []
replaceMin x xs@(y:ys)
	| y > z = [y] ++ replaceMin x ys
	| otherwise = [x] ++ ys
		where z = minimum xs


sort :: (Ord a) => [a] -> [a]
sort [] = []
sort [x] = [x]
sort (x:xs)  
	| x > y = [y] ++ sort (replaceMin x xs)
	| otherwise = [x] ++ sort xs
		where y = minimum xs

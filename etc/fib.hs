fib :: (Integral a) => a -> a
fib 0 = 1
fib 1 = 1
fib n | n > 0 = fib(n - 1) + fib(n - 2)
      | otherwise = error "negatives are not allowed"

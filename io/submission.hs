import Data.Char (toLower)
import Data.Text (isInfixOf)
import Data.String (fromString)

main = submission

submission :: IO () 
submission = do
   putStrLn "Do you obey me?"
   answer <- getLine
   if (containsWord "yes" answer) && not (containsWord "no"  answer) 
	then return ()
	else do
	     putStrLn "Loud punch!"
	     submission

containsWord :: String -> String -> Bool
containsWord word phrase = isInfixOf (fromString word)  (fromString(map toLower phrase))

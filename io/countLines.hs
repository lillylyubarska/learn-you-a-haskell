import Control.Monad
import Data.Char
import System.IO
import System.Environment

countLines :: String -> IO ()
countLines fileName = do
     handle <- openFile fileName ReadMode     
     contents <- hGetContents handle
     let res = length (lines contents)
     print res
     hClose handle


main = do
	args <- getArgs
	res <- countLines (head args)
	print res 
